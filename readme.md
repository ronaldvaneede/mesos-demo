Install docker-compose  
`https://docs.docker.com/compose/install/`

Start all mesos containers  
`docker-compose up`

Mesos  
`http://docker01.vaneede.com:5050`

Marathon  
`http://docker01.vaneede.com:8080`

Consul  
`http://docker01.vaneede.com:8500`

cAdvisor  
`http://docker01.vaneede.com:8880`

Start node app  
`curl -X POST -H "Content-Type: application/json" "localhost:8080/v2/apps?force=true" -d "@node.json"`

Start node app  
`curl -X POST -H "Content-Type: application/json" "localhost:8080/v2/apps?force=true" -d "@elasticsearch.json"`